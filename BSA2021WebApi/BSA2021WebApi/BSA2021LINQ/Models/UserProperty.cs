﻿using BSA2021LINQ.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021LINQ.Models
{
    public class UserProperty
    {
        public UserDTO User { get; set; }
        public TaskDTO Task { get; set; }
    }
}
