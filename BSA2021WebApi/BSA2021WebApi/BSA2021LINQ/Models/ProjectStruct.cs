﻿using BSA2021LINQ.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021LINQ.Models
{
    public class ProjectStruct
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int UsersCount { get; set; }
    }
}
