﻿using BSA2021LINQ.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021LINQ.Models
{
    public class UserStruct
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int TasksCount { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
