﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021LINQ.Models
{
    public class TaskProperty
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
