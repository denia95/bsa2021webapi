﻿using BSA2021LINQ.DTO;
using BSA2021LINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021LINQ.Service
{
    public class LINQService
    {
        public Dictionary<ProjectDTO,int> GetTaskNumberInProject(int userId)
        {
            ClientApi service = new ClientApi();
            var projects = service.GetProjects();
            var tasks = service.GetTasks();
            var dictionary = projects
            .Where(p => p.AuthorId == userId)
            .ToDictionary(p => p, p => tasks.Count(t => t.ProjectId == p.Id));
            return dictionary;
        }

        public List<TaskDTO> GetTaskList(int userId)
        {
            ClientApi service = new ClientApi();
            var tasks = service.GetTasks();
            var list = tasks
            .Where(t => t.PerformerId == userId && t.Name.Length < 45)
            .ToList();
            return list;
        }

        public List<TaskProperty> GetListTaskFromCollections(int userId)
        {
            ClientApi service = new ClientApi();
            var tasks = service.GetTasks();
            var list = tasks
            .Where(t => t.PerformerId == userId && t.FinishedAt.HasValue && t.FinishedAt.Value.Year == DateTime.Today.Year)
            .Select(t => new TaskProperty { Id = t.Id, Name = t.Name })
            .ToList();
            return list;
            
        }

        public List<IGrouping<int,TeamProperty>> GetListTeams()
        {

            ClientApi service = new ClientApi();
            var teams = service.GetTeams();
            var users = service.GetUsers();

            var list = users.Join(teams,
                u => u.TeamId,
                t => t.Id,
                (u, t) => new TeamProperty { Id = t.Id, Name = t.Name, User = u })
                .Where(u => DateTime.Now.Year - u.User.BirthDay.Year > 10)
                .OrderByDescending(u => u.User.RegisteredAt)
                .GroupBy(t => t.Id)
                .ToList();
            return list;
        }

        public List<UserProperty> GetListUsers()
        {
            ClientApi service = new ClientApi();
            var tasks = service.GetTasks();
            var users = service.GetUsers();
            var list = users.Join(tasks,
                u => u.Id,
                t => t.PerformerId,
                (u, t) => new UserProperty { User = u, Task = t })
                .OrderBy(u => u.User.FirstName)
                .ThenByDescending(t => t.Task.Name.Length)
                .ToList();
            return list;
        }

        public  ProjectStruct GetProjectStruct(int projectId)
        {
            ClientApi service = new ClientApi();
            var users = service.GetUsers();
            var tasks = service.GetTasks();
            var project = service.GetProject(projectId);

            return new ProjectStruct
            {
                Project = project,
                LongestTask = tasks.Where(t => t.ProjectId == project.Id).OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestTask = tasks.Where(t => t.ProjectId == project.Id).OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = users.Count(u => u.TeamId == project.TeamId && project.Description.Length > 20 || tasks.Count(t => t.ProjectId == project.Id) < 3)
            };
        }

        public UserStruct GetUserStruct(int userId)
        {
            ClientApi service = new ClientApi();
            var tasks = service.GetTasks();
            var projects = service.GetProjects();
            var user = service.GetUser(userId);

            return new UserStruct
            {
                User = user,
                LastProject = projects.Where(p => p.AuthorId == user.Id)
                    .OrderByDescending(p => p.CreatedAt)
                    .FirstOrDefault(),
                TasksCount = tasks.Count(t => t.ProjectId == projects.Where(p => p.AuthorId == user.Id).OrderByDescending(p => p.CreatedAt).FirstOrDefault()?.Id),
                NotFinishedTasksCount = tasks.Count(t => t.PerformerId == user.Id && t.State != 2),
                LongestTask = tasks.Where(t => t.PerformerId == user.Id).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
            };
        }
    }
}
