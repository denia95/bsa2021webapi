﻿using BSA2021LINQ.Service;
using System;

namespace BSA2021LINQ
{
    public class Program
    {
       
        static void Main(string[] args)
        {
            LINQService service = new LINQService();

            ConsoleKeyInfo keyinfo;

            do
            {
                Console.WriteLine();
                Console.WriteLine("Choose the options below you want to trigger:");
                Console.WriteLine("1 - Get tasks number in project");
                Console.WriteLine("2 - Get list tasks by user id");
                Console.WriteLine("3 - Get list tasks from collection by user id");
                Console.WriteLine("4 - Get list from collections teams, participants older than 10 years");
                Console.WriteLine("5 - Get list of users alphabetically first_name, with sorted tasks by length name");
                Console.WriteLine("6 - Get project structure");
                Console.WriteLine("7 - Get user structure");
                Console.WriteLine();
                int id = 0;
                keyinfo = Console.ReadKey();
                try
                {
                    switch (keyinfo.Key)
                    {
                        case ConsoleKey.D1:
                        case ConsoleKey.NumPad1:
                            Console.WriteLine();
                            id = GetId();
                            var items = service.GetTaskNumberInProject(id);
                            if (items.Count == 0)
                            {
                                Console.WriteLine("There no items for current id");
                            }
                            foreach (var item in items)
                            {
                                Console.WriteLine($"Tasks number in project with name \"{item.Key.Name}\" is: {item.Value}");
                            }
                            break;
                        case ConsoleKey.D2:
                        case ConsoleKey.NumPad2:
                            Console.WriteLine();
                            id = GetId();
                            var items1 = service.GetTaskList(id);
                            if (items1.Count == 0)
                            {
                                Console.WriteLine("There no items for current id");
                            }
                            foreach (var item in items1)
                            {
                                Console.WriteLine($" list of tasks designed for a specific user: TaskName = \"{item.Name}\", Description = \"{item.Description}\" " );
                            }
                            break;
                        case ConsoleKey.D3:
                        case ConsoleKey.NumPad3:
                            Console.WriteLine();
                            id = GetId();
                            var items2 = service.GetListTaskFromCollections(id);
                            if (items2.Count == 0)
                            {
                                Console.WriteLine("There no items for current id");
                            }
                            foreach (var item in items2)
                            {
                                Console.WriteLine($"list from the collection of tasks that are made in the current year for a particular user : TaskName \"{item.Name}\" ");
                            }
                            break;
                        case ConsoleKey.D4:
                        case ConsoleKey.NumPad4:
                            Console.WriteLine();
                            var items3 = service.GetListTeams();
                            if (items3.Count == 0)
                            {
                                Console.WriteLine("There no items for current id");
                            }
                            foreach (var item in items3)
                            {
                                Console.WriteLine($"list from the collection of teams whose members are older than 10 years : {item.Key}, {item.Key}");
                            }
                            break;
                        case ConsoleKey.D5:
                        case ConsoleKey.NumPad5:
                            Console.WriteLine();
                            var items4 = service.GetListUsers();
                            if (items4.Count == 0)
                            {
                                Console.WriteLine("There no items for current id");
                            }
                            foreach (var item in items4)
                            {
                                Console.WriteLine($"list of users in alphabetical order first_name = \"{item.User.FirstName}\" with sorted tasks by length name = \"{item.Task.Name}\" ");
                            }
                            break;
                        case ConsoleKey.D6:
                        case ConsoleKey.NumPad6:
                            Console.WriteLine();
                            id = GetId();
                            var items5 = service.GetProjectStruct(id);
                            if (items5.Project == null)
                            {
                                Console.WriteLine("There no items for current id");
                                break;
                            }
                            Console.WriteLine($"Project structure :");
                            Console.WriteLine($"ProjectName = \"{items5.Project.Name}\", Description = \"{items5.Project.Description}\" ");
                            Console.WriteLine($"Longest task of the project by description = \"{items5.LongestTask.Description}\" ");
                            Console.WriteLine($"The shortest task of the project by name = \"{items5.ShortestTask.Description}\" ");
                            Console.WriteLine($"The total number of users in the project team, where either the project description> 20 characters, or the number of tasks <3 =  = \"{items5.UsersCount}\" ");
                            break;
                        case ConsoleKey.D7:
                        case ConsoleKey.NumPad7:
                            Console.WriteLine();
                            id = GetId();
                            var items6 = service.GetUserStruct(id);
                            if (items6.User == null)
                            {
                                Console.WriteLine("There no items for current id");
                                break;
                            }
                            Console.WriteLine($"User structure :");
                            Console.WriteLine($"UserName = {items6.User.FirstName}, {items6.User.LastName}");
                            Console.WriteLine($"Last user project = {items6.LastProject}");
                            Console.WriteLine($"The total number of tasks under the last project = {items6.TasksCount}");
                            Console.WriteLine($"The total number of incomplete or canceled tasks for the user = {items6.NotFinishedTasksCount}");
                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine("Please choose number from 0-9");
                            break;
                    }
                }
                catch (ArgumentException e) { Console.WriteLine(); Console.WriteLine(e.Message); }
                catch (InvalidOperationException e) { Console.WriteLine(); Console.WriteLine(e.Message); }
                catch (Exception e) { }
            }
            while (keyinfo.Key != ConsoleKey.Escape);

        }
        public static int GetId()
        {
            Console.WriteLine("Please, input id ");
            var s = Console.ReadLine();
            int res = 0;
            try
            {
                res = int.Parse(s);
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong id, try again!");
                GetId();
            }
            return res;
        }
    }
       
}
