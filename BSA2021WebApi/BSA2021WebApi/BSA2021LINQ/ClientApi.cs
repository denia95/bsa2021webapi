﻿using BSA2021LINQ.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace BSA2021LINQ
{
    public class ClientApi
    {
        private const string BASE_PATH = "https://localhost:5001/api/";
        private HttpClient _httpClient;

        public ClientApi()
        {
            _httpClient = new HttpClient();
        }

        public List<ProjectDTO> GetProjects()
        {
            List<ProjectDTO> projects = new List<ProjectDTO>();

            var response = _httpClient.GetAsync(BASE_PATH + "Projects").Result;

            if (response.IsSuccessStatusCode)
            {
                string res = response.Content.ReadAsStringAsync().Result;
                projects = JsonConvert.DeserializeObject<List<ProjectDTO>>(res);
            }
            
            return projects;
        }

        public ProjectDTO GetProject(int id)
        {
            var project = new ProjectDTO();
            
                var response = _httpClient.GetAsync(BASE_PATH + "Projects/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    project = JsonConvert.DeserializeObject<ProjectDTO>(json);
                }
            
            return project;
        }

        public List<TaskDTO> GetTasks()
        {
            List<TaskDTO> tasks = new List<TaskDTO>();
            
            var response = _httpClient.GetAsync(BASE_PATH + "Tasks").Result;

            if (response.IsSuccessStatusCode)
            {
                string res = response.Content.ReadAsStringAsync().Result;
                tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(res);
            }
            
            return tasks;
        }

        public TaskDTO GetTask(int id)
        {
            var task = new TaskDTO();
            
            var response = _httpClient.GetAsync(BASE_PATH + "Tasks/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                task = JsonConvert.DeserializeObject<TaskDTO>(json);
            }
            
            return task;
        }

        public List<TeamDTO> GetTeams()
        {
            List<TeamDTO> teams = new List<TeamDTO>();
           
            var response = _httpClient.GetAsync(BASE_PATH + "Teams").Result;

            if (response.IsSuccessStatusCode)
            {
                string res = response.Content.ReadAsStringAsync().Result;
                teams = JsonConvert.DeserializeObject<List<TeamDTO>>(res);
            }
            
            return teams;
        }

        public TeamDTO GetTeam(int id)
        {
            var team = new TeamDTO();
            
            var response = _httpClient.GetAsync(BASE_PATH + "Teams/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                team = JsonConvert.DeserializeObject<TeamDTO>(json);
            }
            
            return team;
        }

        public List<UserDTO> GetUsers()
        {
            List<UserDTO> users = new List<UserDTO>();
            
            var response = _httpClient.GetAsync(BASE_PATH + "Users").Result;

            if (response.IsSuccessStatusCode)
            {
                string res = response.Content.ReadAsStringAsync().Result;
                users = JsonConvert.DeserializeObject<List<UserDTO>>(res);
            }
           
            return users;
        }

        public UserDTO GetUser(int id)
        {
            var user = new UserDTO();
            
            var response = _httpClient.GetAsync(BASE_PATH + "Users/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                user = JsonConvert.DeserializeObject<UserDTO>(json);
            }
            
            return user;
        }
    }
}
