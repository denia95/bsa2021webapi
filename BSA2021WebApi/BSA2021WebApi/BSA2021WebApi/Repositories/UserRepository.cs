﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021WebApi.Repositories
{
    public class UserRepository : IRepository<User>
    {
        public EntitiesContext context { get; set; }

        public UserRepository()
        {
            context = new EntitiesContext();
        }

        public List<User> ReadEntities()
        {
            return context.Users;
        }

        public User Read(int id)
        {
            return context.Users.FirstOrDefault(x => x.Id == id);
        }

        public void Create(User entity)
        {
            context.Users.Add(entity);
        }

        public void Update(User entity)
        {
            var user = context.Users.FirstOrDefault(x => x.Id == entity.Id);
            context.Users.Remove(user);
            context.Users.Add(entity);
        }

        public void Delete(int id)
        {
            var user = context.Users.FirstOrDefault(x => x.Id == id);
            context.Users.Remove(user);
        }
    }
}
