﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021WebApi
{
    public class ProjectRepository : IRepository<Project>
    {
        public EntitiesContext context { get; set; }

        public ProjectRepository()
        {
            context = new EntitiesContext();
        }

        public List<Project> ReadEntities()
        {
            return context.Projects;
        }

        public Project Read(int id)
        {
            return context.Projects.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Project entity)
        {
            context.Projects.Add(entity);
        }

        public void Update(Project entity)
        {
            var project = context.Projects.FirstOrDefault(x => x.Id == entity.Id);
            context.Projects.Remove(project);
            context.Projects.Add(entity);
        }

        public void Delete(int id)
        {
            var project = context.Projects.FirstOrDefault(x => x.Id == id);
            context.Projects.Remove(project);
        }
    }
}
