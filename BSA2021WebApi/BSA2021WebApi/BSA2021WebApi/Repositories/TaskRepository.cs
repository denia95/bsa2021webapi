﻿using BSA2021WebApi.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021WebApi.Repositories
{
    public class TaskRepository: IRepository<Models.Task>
    {

        public EntitiesContext context { get; set; }

        public TaskRepository()
        {
            context = new EntitiesContext();
        }

        public List<Models.Task> ReadEntities()
        {
            return context.Tasks;
        }

        public Models.Task Read(int id)
        {
            return context.Tasks.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Models.Task entity)
        {
            context.Tasks.Add(entity);
        }

        public void Update(Models.Task entity)
        {
            var task = context.Tasks.FirstOrDefault(x => x.Id == entity.Id);
            context.Tasks.Remove(task);
            context.Tasks.Add(entity);
        }

        public void Delete(int id)
        {
            var task = context.Tasks.FirstOrDefault(x => x.Id == id);
            context.Tasks.Remove(task);
        }

    }
}
