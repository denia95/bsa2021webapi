﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021WebApi.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        public EntitiesContext context { get; set; }

        public TeamRepository()
        {
            context = new EntitiesContext();
        }

        public List<Team> ReadEntities()
        {
            return context.Teams;
        }

        public Team Read(int id)
        {
            return context.Teams.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Team entity)
        {
            context.Teams.Add(entity);
        }

        public void Update(Team entity)
        {
            var team = context.Teams.FirstOrDefault(x => x.Id == entity.Id);
            context.Teams.Remove(team);
            context.Teams.Add(entity);
        }

        public void Delete(int id)
        {
            var team = context.Teams.FirstOrDefault(x => x.Id == id);
            context.Teams.Remove(team);
        }
    }
}
