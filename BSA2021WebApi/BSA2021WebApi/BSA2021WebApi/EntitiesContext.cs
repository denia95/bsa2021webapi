﻿using BSA2021WebApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace BSA2021WebApi
{
    public class EntitiesContext
    {
        public List<Project> Projects { get; set; }
        public List<Models.Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }

        public EntitiesContext()
        {
            Projects = GetEntity<Project>("projects.json");
            Tasks = GetEntity<Models.Task>("tasks.json");
            Teams = GetEntity<Team>("teams.json");
            Users = GetEntity<User>("users.json");
        }

        public List<T> GetEntity<T>(string fileName)
        {
            List<T> res = new List<T>();

            using (StreamReader sr = new StreamReader(fileName))
            {
                var file = sr.ReadToEnd();
                res = JsonConvert.DeserializeObject<List<T>>(file);
            };

            return res;
        }
    }
}
