﻿using BSA2021WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : Controller
    {
        private IRepository<Models.Task> _repository;
        public TasksController(IRepository<Models.Task> repository)
        {
            _repository = repository;
        }

        public List<Models.Task> Get()
        {
            var entites = _repository.ReadEntities();
            return entites;
        }

        [HttpGet("{id:int}")]
        public Models.Task Get(int id)
        {
            var entity = _repository.Read(id);
            return entity;
        }
    }
}
