﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : Controller
    {
        private IRepository<Team> _repository;
        public TeamsController(IRepository<Team> repository)
        {
            _repository = repository;
        }

        public List<Team> Get()
        {
            var entites = _repository.ReadEntities();
            return entites;
        }

        [HttpGet("{id:int}")]
        public Team Get(int id)
        {
            var entity = _repository.Read(id);
            return entity;
        }
    }
}
