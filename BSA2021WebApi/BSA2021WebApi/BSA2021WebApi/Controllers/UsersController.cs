﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA2021WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IRepository<User> _repository;
        public UsersController(IRepository<User> repository)
        {
            _repository = repository;
        }

        public List<User> Get()
        {
            var entites = _repository.ReadEntities();
            return entites;
        }

        [HttpGet("{id:int}")]
        public User Get(int id)
        {
            var entity = _repository.Read(id);
            return entity;
        }
    }
}
