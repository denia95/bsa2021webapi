﻿using BSA2021WebApi.Interfaces;
using BSA2021WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {
        private IRepository<Project> _repository;
        public ProjectsController(IRepository<Project> repository)
        {
            _repository = repository;
        }

        public List<Project> Get()
        {
            var entites = _repository.ReadEntities();
            return entites;
        }

        [HttpGet("{id:int}")]
        public Project Get(int id)
        {
            var entity = _repository.Read(id);
            return entity;
        }
    }
}
