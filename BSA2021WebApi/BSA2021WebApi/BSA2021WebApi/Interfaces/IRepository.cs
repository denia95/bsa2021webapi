﻿using System.Collections.Generic;

namespace BSA2021WebApi.Interfaces
{
    public interface IRepository<T> where T: class
    {
        List<T> ReadEntities(); 
        T Read(int id);
        void Create(T entity); 
        void Update(T entity); 
        void Delete(int id);
    }
}
